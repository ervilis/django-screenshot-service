# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Screenshot',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('url', models.URLField()),
                ('image', models.ImageField(upload_to=b'', null=True, verbose_name='Image', blank=True)),
                ('date_insert', models.DateTimeField(auto_now_add=True, verbose_name='Date insert')),
            ],
        ),
    ]
