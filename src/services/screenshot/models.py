from django.db import models
from django.utils.translation import ugettext_lazy as _

from .signals import save_screenshot


class Screenshot(models.Model):
    url = models.URLField()
    image = models.ImageField(_(u"Image"), null=True, blank=True)
    date_insert = models.DateTimeField(_(u"Date insert"), auto_now_add=True)

    __unicode__ = lambda x: x.url


models.signals.pre_save.connect(save_screenshot, sender=Screenshot)
