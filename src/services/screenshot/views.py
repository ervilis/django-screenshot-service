from django.shortcuts import get_object_or_404
from django.http import Http404

from rest_framework import viewsets, mixins, response, status

from .serializers import ScreenshotSerializer
from .models import Screenshot


class ScreenshotViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    """A viewset that provides list and create interface to Screenshots"""

    queryset = Screenshot.objects.all()
    serializer_class = ScreenshotSerializer
    filter_fields = ('url',)

    def post(self, request, *args, **kwargs):
        """
        API not allow create two screenshots with same url, in this case
        returns the same object with the respective status_code 200 or 201.
        """
        try:
            instance = self.get_object()
            serializer = self.get_serializer(instance)
            return response.Response(serializer.data)
        except Http404:
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            serializer.save()
            return response.Response(serializer.data,
                                     status=status.HTTP_201_CREATED)

    def get_object(self):
        queryset = self.filter_queryset(self.get_queryset())
        serializer = self.get_serializer(data=self.request.data)
        serializer.is_valid(raise_exception=True)

        obj = get_object_or_404(queryset, url=serializer.initial_data['url'])

        return obj
