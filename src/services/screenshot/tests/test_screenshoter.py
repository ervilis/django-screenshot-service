import unittest
import tempfile
import mock

from django.core.exceptions import ValidationError

from services.screenshot.screenshoter import take_screenshot


class TakeScreenshotTestCase(unittest.TestCase):

    def test_successful(self):
        # Apply mock to not execute command
        with mock.patch("services.screenshot.screenshoter.subprocess") as sp:
            sp.Popen.return_value.returncode = 0
            temp_path = tempfile.mkstemp()[1]
            self.assertTrue(take_screenshot("http://google.com/", temp_path))

    def test_invalid_destination_dir(self):
        with self.assertRaises(IOError):
            take_screenshot("http://foo.bar/img.jpg", "/invalid_dir/out.jpg")

    def test_invalid_url(self):
        with self.assertRaises(ValidationError):
            take_screenshot("http://invalid_url", "/invalid_dir/out.jpg")
