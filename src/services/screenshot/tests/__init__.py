"""
We need disconnect the save_screenshot signal for run another tests
"""
from django.db.models import signals

from services.screenshot.models import Screenshot
from services.screenshot.signals import save_screenshot


signals.pre_save.disconnect(save_screenshot, sender=Screenshot)
