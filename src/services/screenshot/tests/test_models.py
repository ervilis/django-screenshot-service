from tempfile import NamedTemporaryFile
from datetime import datetime

from django.test import TestCase
from django.core.files import File

from services.screenshot.models import Screenshot


class ScreenshotTestCase(TestCase):

    def test_instance(self):
        self.assertIsInstance(Screenshot(), Screenshot)

    def test_instance_save(self):
        instance = Screenshot(url='https://fo.bar/img.jpg',
                              image=File(NamedTemporaryFile(mode='rb')))
        instance.save()
        self.assertTrue(instance.pk)
        self.assertIsInstance(instance.image.url, (str, unicode))
        self.assertIsInstance(instance.date_insert, datetime)
