import json

from mockups import Mockup

from django.test import TestCase, Client

from rest_framework.reverse import reverse

from services.screenshot.models import Screenshot


RESOURCE_URL = reverse('screenshot-list')

client = Client()


class ScreenshotListTestCase(TestCase):

    def setUp(self):
        self.instances = Mockup(Screenshot).create(3)

    def test_list(self):
        """Create instances of mockup and certify that they are returned"""
        response = client.get(RESOURCE_URL)
        data = json.loads(response.content)

        self.assertEquals(response.status_code, 200)

        for instance in self.instances:
            self.assertTrue(filter(lambda x: x['url'] == instance.url, data))


class ScreenshotCreateTestCase(TestCase):

    def test_create_successful(self):
        post_data = {
            'url': 'http://foo.bar/img.jpg',
        }
        response = client.post(RESOURCE_URL,
                               json.dumps(post_data),
                               content_type='application/json')
        response_data = json.loads(response.content)

        self.assertEquals(response.status_code, 201)
        self.assertEqual(response_data['url'], post_data['url'])

    def test_create_duplicate(self):
        """
        API not allow create two screenshot with same url, in this case
        returns the same objects with the respective status_code 200 or 201.
        """
        post_data = {
            'url': 'http://foo.bar/img.jpg',
        }
        response = client.post(RESOURCE_URL,
                               json.dumps(post_data),
                               content_type='application/json')
        response_data1 = json.loads(response.content)

        response2 = client.post(RESOURCE_URL,
                                json.dumps(post_data),
                                content_type='application/json')
        response_data2 = json.loads(response.content)

        self.assertEquals(response.status_code, 201)
        self.assertEquals(response2.status_code, 200)
        self.assertEqual(response_data1, response_data2)

    def test_validation_error(self):
        response = client.post(RESOURCE_URL,
                               json.dumps({}),
                               content_type='application/json')
        response_data = json.loads(response.content)

        self.assertEquals(response.status_code, 400)
        self.assertTrue('url' in response_data.keys())
        self.assertEqual(response_data['url'][0], "This field is required.")


class ScreenshotNotAllowedMethodTestCase(TestCase):

    def test_retrieve(self):
        instance = Mockup(Screenshot).create_one()
        response = Client().get("{}{}".format(RESOURCE_URL, instance.pk))
        self.assertEquals(response.status_code, 404)

    def test_delete(self):
        response = Client().delete(RESOURCE_URL)
        self.assertEquals(response.status_code, 405)


class ScreenshotFilterTestCase(TestCase):

    def setUp(self):
        self.instances = Mockup(Screenshot).create(3)

    def test_filter_successful(self):
        url = "{}?url={}".format(RESOURCE_URL, self.instances[0].url)
        response = client.get(url)
        data = json.loads(response.content)

        self.assertEquals(response.status_code, 200)
        self.assertEquals(len(data), 1)
        self.assertEquals(data[0]['url'], self.instances[0].url)

    def test_filter_not_found(self):
        url = "{}?url={}".format(RESOURCE_URL, 'http://INotExist.com/img.jpg')
        response = client.get(url)
        data = json.loads(response.content)

        self.assertEquals(response.status_code, 200)
        self.assertEquals(len(data), 0)
