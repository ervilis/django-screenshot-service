import mock
from tempfile import NamedTemporaryFile

from django.test import TestCase
from django.core.files import File

from services.screenshot.models import Screenshot
from services.screenshot.signals import save_screenshot


class ScreenshotTestCase(TestCase):

    def test_save(self):
        instance = Screenshot(url='https://fo.bar/')

        patch1 = "services.screenshot.signals.take_screenshot"
        patch2 = "__builtin__.open"
        with mock.patch(patch1) as p1, mock.patch(patch2) as p2:
            p1.return_value = True
            p2.return_value = NamedTemporaryFile(mode='rb')

            save_screenshot(Screenshot, instance)

        instance.save()

        self.assertTrue(instance.pk)
        self.assertIsInstance(instance.image.url, (str, unicode))
