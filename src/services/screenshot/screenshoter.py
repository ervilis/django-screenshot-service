import os.path
import subprocess

from django.core.validators import URLValidator
from django.utils.translation import ugettext_lazy as _


def take_screenshot(url, destination):
    """
    Function to take a screenshot of an webpage using cutycapt to generate an
    image and xvfb to emulate an window.
    """
    # URLValidator raises a ValidationError when url is incorrect
    URLValidator()(url)

    destination_dir = os.path.dirname(destination)
    if not os.path.exists(destination_dir):
        raise IOError(_(u"No such directory '{}'").format(destination_dir))

    xvfb_command = 'xvfb-run --server-args="-screen 0, 1024x768x24"'
    cutycapt_command = 'cutycapt --url={} --out={}'.format(url, destination)
    command = "{} {}".format(xvfb_command, cutycapt_command)

    process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
    process.wait()
    return process.returncode == 0
