# -*- coding: utf-8 -*-
from rest_framework import serializers
from .models import Screenshot


class ScreenshotSerializer(serializers.ModelSerializer):

    class Meta:
        model = Screenshot
        extra_kwargs = {'image': {'read_only': True}}
