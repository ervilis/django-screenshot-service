from rest_framework import routers

from .views import ScreenshotViewSet


router = routers.SimpleRouter()
router.register(r'screenshot', ScreenshotViewSet)

urlpatterns = router.urls
