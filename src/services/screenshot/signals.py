import uuid
import tempfile

from django.core.files import File

from .screenshoter import take_screenshot


def save_screenshot(sender, instance, **kwargs):
    if instance.image:
        return

    destination = "{}/{}.png".format(tempfile.gettempdir(), uuid.uuid4())
    take_screenshot(instance.url, destination)
    instance.image = File(open(destination, 'r'))
