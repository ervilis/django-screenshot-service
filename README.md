## Django Web Screenshot API service

Project that provides REST API to create website screenshots.

### Requirements
Project uses cutycapt for create screenshots, so you have to install cutycapt and xvfb.

`sudo apt-get install cutycapt xvfb`


System requirements:

`sudo apt-get install python-dev python-pip build-essential`


### Running application
`virtualenv .venv`

`source .venv/bin/activate`

`pip install -r requirements.txt`

`python src/manage.py syncdb`

`python src/manage.py collectstatic --noinput`

`cd src/; gunicorn services.wsgi:application --bind 0.0.0.0:8000`


### Running tests

`pip install -r requirements_dev.txt`

`cd src; python manage.py test`
